/**
 * The NegativeException class is meant to be a custom Exception class
 * that reports the exception of the user inputting a negative GPA
 * for the Applicant 
 */
public class NegativeException extends Exception
{
     /**
     * Constructor of the NegativeException, it takes the a input of a
     * msg that is printed when the exception is evoked
     * 
     * @param msg 
     *  The message to display to the user when the exception is called
     */
    public NegativeException(String msg)
    {
        super(msg);
    }
}
